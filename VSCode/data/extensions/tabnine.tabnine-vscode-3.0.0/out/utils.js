"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.once = void 0;
function once(key, context) {
    return new Promise(function (resolve) {
        if (!context.globalState.get(key)) {
            context.globalState.update(key, true).then(resolve);
        }
    });
}
exports.once = once;
//# sourceMappingURL=utils.js.map