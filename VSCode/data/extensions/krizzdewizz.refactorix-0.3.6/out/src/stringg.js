"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vs = require("vscode");
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function interpolate() {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const changes = core_1.interpolate(source.sourceFile, refactor_1.selectionToSpan(document, selection));
    if (!changes) {
        return;
    }
    editor.edit(builder => changes.forEach(change => builder.replace(refactor_1.changeToRange(document, change), change.newText)))
        .then(ok => {
        if (ok) {
            editor.selection = new vs.Selection(new vs.Position(selection.start.line, selection.start.character + core_1.INTERPOLATE_PREFIX_LEN), new vs.Position(selection.start.line, selection.end.character + core_1.INTERPOLATE_PREFIX_LEN));
        }
    });
}
exports.interpolate = interpolate;
//# sourceMappingURL=stringg.js.map