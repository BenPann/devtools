"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vs = require("vscode");
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function semicolons(add) {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const changes = core_1.semicolons(source.sourceFile, add);
    if (changes.length === 0) {
        return;
    }
    editor.edit(builder => {
        const doIt = add
            ? change => builder.insert(document.positionAt(change), ';')
            : change => builder.replace(new vs.Range(document.positionAt(change - 1), document.positionAt(change)), '');
        changes.forEach(doIt);
    }).then(ok => {
        if (ok) {
            editor.selection = selection;
        }
    });
}
exports.semicolons = semicolons;
//# sourceMappingURL=semicolon.js.map