"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const refactor_1 = require("./refactor");
const selection_1 = require("./selection");
function createSourceRange(offset, end) {
    let length = end - offset + 1;
    if (length === 0) {
        length = 1;
    }
    return { offset: Math.max(0, offset), length };
}
function getSelectedNodeSourceRange(sourceReferenceLength, nodeToSelect) {
    const offset = nodeToSelect.getStart();
    const end = Math.min(sourceReferenceLength, nodeToSelect.getEnd());
    return createSourceRange(offset, end);
}
function getLastCoveringNodeRange(oldSourceRange, sourceReferenceLength, selAnalyzer) {
    if (selAnalyzer.lastCoveringNode) {
        return getSelectedNodeSourceRange(sourceReferenceLength, selAnalyzer.lastCoveringNode);
    }
    else {
        return oldSourceRange;
    }
}
function cleverSel(grow) {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const s = selection_1.XSelection.createFromStartEnd(selection.start.character, selection.end.character);
    const ana = selection_1.analyzeSelection(source.sourceFile, s);
    function internalGetNewSelectionRange(oldSourceRange, sr, selAnalyzer) {
        const first = ana.firstSelectedNode;
        if (!first || !first.parent) {
            return getLastCoveringNodeRange(oldSourceRange, sr, selAnalyzer);
        }
        return getSelectedNodeSourceRange(sr, first.parent);
    }
    const oldSourceRange = { offset: selection.start.character, length: selection.end.character - selection.start.character };
    const newSel = internalGetNewSelectionRange(oldSourceRange, 1000, ana);
    console.log('sele', newSel);
}
exports.cleverSel = cleverSel;
//# sourceMappingURL=clever-sel.js.map