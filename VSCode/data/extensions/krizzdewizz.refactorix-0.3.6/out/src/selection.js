"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
var Mode;
(function (Mode) {
    Mode[Mode["INTERSECTS"] = 0] = "INTERSECTS";
    Mode[Mode["BEFORE"] = 1] = "BEFORE";
    Mode[Mode["SELECTED"] = 2] = "SELECTED";
    Mode[Mode["AFTER"] = 3] = "AFTER";
})(Mode = exports.Mode || (exports.Mode = {}));
function analyzeSelection(sourceFile, fSelection) {
    let fSelectedNodes;
    let lastCoveringNode;
    visitor(sourceFile);
    return { firstSelectedNode: getFirstSelectedNode(), lastCoveringNode };
    function isFirstNode() {
        return fSelectedNodes === undefined;
    }
    function handleFirstSelectedNode(node) {
        fSelectedNodes = [node];
    }
    function handleNextSelectedNode(node) {
        if (getFirstSelectedNode().parent === node.parent) {
            fSelectedNodes.push(node);
        }
    }
    function getFirstSelectedNode() {
        return fSelectedNodes[0];
    }
    function visitNode(node) {
        // The selection lies behind the node.
        if (fSelection.liesOutside(node)) {
            return false;
        }
        else if (fSelection.coversNode(node)) {
            if (isFirstNode()) {
                handleFirstSelectedNode(node);
            }
            else {
                handleNextSelectedNode(node);
            }
            return true; //fTraverseSelectedNode;
        }
        else if (fSelection.coveredByNode(node)) {
            lastCoveringNode = node;
            return true;
        }
        else if (fSelection.endsIn(node)) {
            // return handleSelectionEndsIn(node);
            return false;
        }
        // There is a possibility that the user has selected trailing semicolons that don't belong
        // to the statement. So dive into it to check if sub nodes are fully covered.
        return true;
    }
    function visitor(node) {
        if (visitNode(node)) {
            ts.forEachChild(node, visitor);
        }
    }
}
exports.analyzeSelection = analyzeSelection;
class XSelection {
    static createFromStartLength(s, l) {
        const result = new XSelection();
        result.fStart = s;
        result.fLength = l;
        result.fExclusiveEnd = s + l;
        return result;
    }
    static createFromStartEnd(s, e) {
        const result = new XSelection();
        result.fStart = s;
        result.fLength = e - s + 1;
        result.fExclusiveEnd = result.fStart + result.fLength;
        return result;
    }
    getOffset() {
        return this.fStart;
    }
    getLength() {
        return this.fLength;
    }
    getInclusiveEnd() {
        return this.fExclusiveEnd - 1;
    }
    getExclusiveEnd() {
        return this.fExclusiveEnd;
    }
    getVisitSelectionMode(node) {
        const nodeStart = node.getStart();
        const nodeEnd = node.getEnd();
        if (nodeEnd <= this.fStart) {
            return Mode.BEFORE;
        }
        else if (this.coversNode(node)) {
            return Mode.SELECTED;
        }
        else if (this.fExclusiveEnd <= nodeStart) {
            return Mode.AFTER;
        }
        return Mode.INTERSECTS;
    }
    getEndVisitSelectionMode(node) {
        const nodeStart = node.getStart();
        const nodeEnd = node.getEnd();
        if (nodeEnd <= this.fStart) {
            return Mode.BEFORE;
        }
        else if (this.coversNode(node)) {
            return Mode.SELECTED;
        }
        else if (nodeEnd >= this.fExclusiveEnd) {
            return Mode.AFTER;
        }
        return Mode.INTERSECTS;
    }
    // cover* methods do a closed interval check.
    coversPosition(position) {
        return this.fStart <= position && position < this.fStart + this.fLength;
    }
    coversNode(node) {
        const nodeStart = node.getStart();
        return this.fStart <= nodeStart && node.getEnd() <= this.fExclusiveEnd;
    }
    coveredByNode(node) {
        const nodeStart = node.getStart();
        return nodeStart <= this.fStart && this.fExclusiveEnd <= node.getEnd();
    }
    // coveredByRegion(region: IRegion): boolean {
    //     const rangeStart = region.getOffset();
    //     return rangeStart <= this.fStart && this.fExclusiveEnd <= rangeStart + region.getLength();
    // }
    endsIn(node) {
        const nodeStart = node.getStart();
        return nodeStart < this.fExclusiveEnd && this.fExclusiveEnd < node.getEnd();
    }
    liesOutside(node) {
        const nodeStart = node.getStart();
        const nodeEnd = node.getEnd();
        const nodeBeforeSelection = nodeEnd < this.fStart;
        const selectionBeforeNode = this.fExclusiveEnd < nodeStart;
        return nodeBeforeSelection || selectionBeforeNode;
    }
}
exports.XSelection = XSelection;
//# sourceMappingURL=selection.js.map