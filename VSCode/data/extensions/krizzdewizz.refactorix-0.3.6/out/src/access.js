"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vs = require("vscode");
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function toggle() {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const options = vs.workspace.getConfiguration('extension.refactorix.Access.toggle');
    const changes = core_1.toggle(source.sourceFile, document.offsetAt(selection.start), options);
    if (!changes) {
        return;
    }
    editor.edit(builder => changes.forEach(change => builder.replace(refactor_1.changeToRange(document, change), change.newText)))
        .then(ok => {
        if (ok) {
            editor.selection = selection;
        }
    });
}
exports.toggle = toggle;
//# sourceMappingURL=access.js.map