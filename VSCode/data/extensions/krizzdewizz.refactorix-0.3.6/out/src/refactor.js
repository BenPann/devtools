"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const vs = require("vscode");
const core_1 = require("./core");
function getTabs(editor, nTabs) {
    return (editor.options.insertSpaces ? ' ' : '\t').repeat(Number(editor.options.tabSize) * nTabs);
}
exports.getTabs = getTabs;
function getIndentAtLine(doc, line) {
    const lineText = doc.getText(new vs.Range(new vs.Position(line, 0), new vs.Position(line, 30)));
    return core_1.getIndent(lineText);
}
exports.getIndentAtLine = getIndentAtLine;
function selectionToSpan(doc, sel) {
    return { start: doc.offsetAt(sel.start), length: doc.offsetAt(sel.end) - doc.offsetAt(sel.start) };
}
exports.selectionToSpan = selectionToSpan;
function changeToRange(doc, change) {
    return new vs.Range(doc.positionAt(change.span.start), doc.positionAt(change.span.start + change.span.length));
}
exports.changeToRange = changeToRange;
function createSourceFileFromActiveEditor() {
    const editor = vs.window.activeTextEditor;
    if (!editor) {
        return undefined;
    }
    const doc = editor.document;
    const sourceFile = ts.createSourceFile(doc.fileName, doc.getText(), ts.ScriptTarget.Latest, true);
    return { editor, sourceFile };
}
exports.createSourceFileFromActiveEditor = createSourceFileFromActiveEditor;
//# sourceMappingURL=refactor.js.map