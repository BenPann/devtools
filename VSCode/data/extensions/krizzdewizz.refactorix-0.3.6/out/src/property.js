"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vs = require("vscode");
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function toGetterSetter() {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const options = vs.workspace.getConfiguration('extension.refactorix.Property.ToGetterSetter');
    const change = core_1.toGetterSetter(source.sourceFile, document.offsetAt(selection.start), refactor_1.getIndentAtLine(document, selection.start.line), refactor_1.getTabs(editor, 1), options);
    if (!change) {
        return;
    }
    editor.edit(builder => builder.replace(refactor_1.changeToRange(document, change), change.newText))
        .then(ok => {
        if (ok) {
            editor.selection = selection;
        }
    });
}
exports.toGetterSetter = toGetterSetter;
//# sourceMappingURL=property.js.map