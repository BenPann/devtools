"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const arrow_function_1 = require("./arrow-function");
const extract_variable_1 = require("./extract-variable");
const semicolon_1 = require("./semicolon");
const property_1 = require("./property");
const stringg_1 = require("./stringg");
const access_1 = require("./access");
const split_variable_declaration_1 = require("./split-variable-declaration");
function activate(context) {
    context.subscriptions.push(vscode.commands.registerCommand('extension.refactorix.SplitVariableDeclaration', split_variable_declaration_1.splitVariableDeclaration), vscode.commands.registerCommand('extension.refactorix.ExtractVariable', extract_variable_1.extractVariable), vscode.commands.registerCommand('extension.refactorix.ArrowFunction.ToggleSingleStatementBlockExpression', arrow_function_1.toggleSingleStatementBlockExpression), vscode.commands.registerCommand('extension.refactorix.ArrowFunction.SingleStatementBlockToExpressionAll', () => arrow_function_1.singleStatementBlockToExpression(true)), vscode.commands.registerCommand('extension.refactorix.ArrowFunction.ExpressionToBlock', arrow_function_1.expressionToBlock), vscode.commands.registerCommand('extension.refactorix.Semicolons.Add', () => semicolon_1.semicolons(true)), vscode.commands.registerCommand('extension.refactorix.Semicolons.Remove', () => semicolon_1.semicolons(false)), vscode.commands.registerCommand('extension.refactorix.Property.ToGetterSetter', property_1.toGetterSetter), vscode.commands.registerCommand('extension.refactorix.String.Interpolate', stringg_1.interpolate), vscode.commands.registerCommand('extension.refactorix.Access.toggle', access_1.toggle));
}
exports.activate = activate;
//# sourceMappingURL=extension.js.map