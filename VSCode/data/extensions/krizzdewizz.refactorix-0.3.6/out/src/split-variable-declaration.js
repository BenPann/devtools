"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vs = require("vscode");
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function splitVariableDeclaration() {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    core_1.splitVariableDeclaration(source.sourceFile, document, refactor_1.selectionToSpan(document, selection), refactor_1.getIndentAtLine(document, selection.start.line)).then(change => {
        if (!change) {
            return;
        }
        editor.edit(builder => builder.replace(refactor_1.changeToRange(document, change.change), change.change.newText))
            .then(ok => {
            if (ok) {
                const sel = change.selection;
                if (sel) {
                    editor.selection = new vs.Selection(document.positionAt(sel.start), document.positionAt(sel.start + sel.length));
                }
                else {
                    const nextLine = selection.start.line + 1;
                    const lastCol = 10000;
                    editor.selection = new vs.Selection(nextLine, lastCol, nextLine, lastCol);
                }
            }
        });
    });
}
exports.splitVariableDeclaration = splitVariableDeclaration;
//# sourceMappingURL=split-variable-declaration.js.map