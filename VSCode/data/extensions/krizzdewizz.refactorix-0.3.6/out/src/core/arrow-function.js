"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const refactor_1 = require("./refactor");
const RETURN = 'return ';
const ARROW = '=>';
function expressionToBlock(sourceFile, range, indent, tab) {
    const text = sourceFile.getFullText();
    let change;
    visitor(sourceFile);
    return change;
    function visitor(node) {
        if (node.kind === ts.SyntaxKind.ArrowFunction) {
            if (!refactor_1.findChildOfKind(node, ts.SyntaxKind.Block) && refactor_1.inRange(node, range)) {
                const nodeText = text.substring(node.getStart(), node.getEnd());
                const pos = nodeText.indexOf(ARROW);
                const expr = nodeText.substring(pos + ARROW.length).trim();
                const newText = nodeText.substring(0, pos) + ARROW + ' {\n' + indent + tab + 'return ' + expr + ';\n' + indent + '}';
                change = { span: { start: node.getStart(), length: node.getEnd() - node.getStart() }, newText };
            }
        }
        if (!change) {
            ts.forEachChild(node, visitor);
        }
    }
}
exports.expressionToBlock = expressionToBlock;
function singleStatementBlockToExpressions(sourceFile, range) {
    const text = sourceFile.getFullText();
    const changes = [];
    let overlaps = false;
    visitor(sourceFile);
    return { changes, overlaps };
    function visitor(node) {
        if (node.kind === ts.SyntaxKind.ArrowFunction) {
            const block = refactor_1.findChildOfKind(node, ts.SyntaxKind.Block);
            if (block) {
                const children = refactor_1.childrenOf(block);
                if (children.length === 1) {
                    const first = children[0];
                    if ((first.kind === ts.SyntaxKind.ReturnStatement || first.kind === ts.SyntaxKind.ExpressionStatement) && refactor_1.inRange(node, range)) {
                        let newText = text.substring(first.getStart(), first.getEnd());
                        if (newText.endsWith(';')) {
                            newText = newText.substring(0, newText.length - 1);
                        }
                        if (newText.startsWith(RETURN)) {
                            newText = newText.substring(RETURN.length);
                        }
                        const change = { span: { start: block.getStart(), length: block.getEnd() - block.getStart() }, newText };
                        if (refactor_1.hasOverlaps(change, changes)) {
                            overlaps = true;
                        }
                        else {
                            changes.push(change);
                        }
                    }
                }
            }
        }
        ts.forEachChild(node, visitor);
    }
}
exports.singleStatementBlockToExpressions = singleStatementBlockToExpressions;
//# sourceMappingURL=arrow-function.js.map