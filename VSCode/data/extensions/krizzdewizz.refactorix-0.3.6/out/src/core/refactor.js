"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
;
function getIndent(text) {
    let indent = '';
    for (let i = 0, n = text.length; i < n; i++) {
        const c = text[i];
        if (c === ' ' || c === '\t') {
            indent += c;
        }
        else {
            break;
        }
    }
    return indent;
}
exports.getIndent = getIndent;
function findChildOfKind(node, kind) {
    return node.getChildren().find(it => it.kind === kind);
}
exports.findChildOfKind = findChildOfKind;
function childrenOf(node) {
    const all = [];
    ts.forEachChild(node, it => all.push(it));
    return all;
}
exports.childrenOf = childrenOf;
function contains(span, pos) {
    return pos >= span.start && pos <= span.start + span.length;
}
exports.contains = contains;
function hasOverlaps(change, all) {
    const start = change.span.start;
    const end = start + change.span.length;
    return all.map(it => it.span).some(it => contains(it, start) || contains(it, end));
}
exports.hasOverlaps = hasOverlaps;
function inRange(node, range) {
    if (!range) {
        return true;
    }
    return node.getStart() < range.start && node.getEnd() > range.start + range.length;
}
exports.inRange = inRange;
//# sourceMappingURL=refactor.js.map