"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const refactor_1 = require("./refactor");
function cleverSel(grow) {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return;
    }
    const editor = source.editor;
    const { document, selection } = editor;
}
exports.cleverSel = cleverSel;
//# sourceMappingURL=clever-sel.js.map