"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const refactor_1 = require("./refactor");
function isArrowFunctionPropertyWithBlock(node) {
    if (node.kind !== ts.SyntaxKind.PropertyDeclaration) {
        return false;
    }
    let found;
    ts.forEachChild(node, visitor);
    return found;
    function visitor(node) {
        if (node.kind === ts.SyntaxKind.ArrowFunction && refactor_1.findChildOfKind(node, ts.SyntaxKind.Block)) {
            found = true;
        }
        if (!found) {
            ts.forEachChild(node, visitor);
        }
    }
}
function semicolons(sourceFile, add) {
    const changes = [];
    const text = sourceFile.getFullText();
    visitor(sourceFile);
    return changes;
    function checkSemi(node) {
        if (isArrowFunctionPropertyWithBlock(node)) {
            return;
        }
        const last = text.substr(node.getEnd() - 1, 1);
        const semi = last === ';';
        if (add && !semi || !add && semi) {
            changes.push(node.getEnd());
        }
    }
    function visitor(node) {
        switch (node.kind) {
            case ts.SyntaxKind.VariableStatement:
            case ts.SyntaxKind.ExpressionStatement:
            case ts.SyntaxKind.ReturnStatement:
            case ts.SyntaxKind.BreakStatement:
            case ts.SyntaxKind.ContinueStatement:
            case ts.SyntaxKind.ThrowStatement:
            case ts.SyntaxKind.ImportDeclaration:
            case ts.SyntaxKind.ImportEqualsDeclaration:
            case ts.SyntaxKind.DoStatement:
            case ts.SyntaxKind.DebuggerStatement:
            case ts.SyntaxKind.PropertyDeclaration:
                checkSemi(node);
                break;
            case ts.SyntaxKind.InterfaceDeclaration:
                node.members.forEach(checkSemi);
                break;
        }
        ts.forEachChild(node, visitor);
    }
}
exports.semicolons = semicolons;
//# sourceMappingURL=semicolon.js.map