"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const PREFIX = '${';
const SUFFIX = '}';
const BACKTICK = '`';
exports.INTERPOLATE_PREFIX_LEN = PREFIX.length;
function interpolate(sourceFile, range) {
    const text = sourceFile.getFullText();
    let changes;
    visitor(sourceFile);
    return changes;
    function visitor(node) {
        if (node.kind === ts.SyntaxKind.StringLiteral || node.kind === ts.SyntaxKind.FirstTemplateToken) {
            const span = { start: node.getStart(), length: node.getEnd() - node.getStart() };
            if (ts.textSpanContainsTextSpan(span, range)) {
                const rangeText = text.substr(range.start, range.length);
                const newText = PREFIX + rangeText + SUFFIX;
                changes = [{ span: range, newText }];
                if (node.kind === ts.SyntaxKind.StringLiteral) {
                    changes = [
                        ...changes,
                        { span: { start: node.getStart(), length: BACKTICK.length }, newText: BACKTICK },
                        { span: { start: node.getEnd() - BACKTICK.length, length: BACKTICK.length }, newText: BACKTICK }
                    ];
                }
            }
        }
        if (!changes) {
            ts.forEachChild(node, visitor);
        }
    }
}
exports.interpolate = interpolate;
//# sourceMappingURL=stringg.js.map