"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const vs = require("vscode");
const refactor_1 = require("./refactor");
function narrowType(type) {
    if (type.startsWith('"') && type.endsWith('"')) {
        return 'string';
    }
    const first = type[0];
    if (first >= '0' && first <= '9') {
        return 'number';
    }
    if (type === 'true' || type === 'false') {
        return 'boolean';
    }
    return type;
}
function resolveType(expr, doc) {
    return new Promise(resolve => {
        const def = () => resolve(undefined);
        if (expr) {
            vs.commands.executeCommand('vscode.executeHoverProvider', doc.uri, doc.positionAt(expr.getStart())).then(val => {
                const hovers = Array.isArray(val) ? val : [val];
                let value;
                hovers.find(hover => {
                    hover.contents.find(ms => {
                        if (typeof ms === 'string') {
                            value = ms;
                        }
                        else if (ms.language === 'typescript') {
                            value = ms.value;
                        }
                        return Boolean(value);
                    });
                    return Boolean(value);
                });
                if (value) {
                    const pos = value.indexOf(':');
                    if (pos >= 0) {
                        const type = value.substring(pos + 1).trim();
                        resolve(narrowType(type));
                        return;
                    }
                }
                def();
            });
        }
        else {
            def();
        }
    });
}
function splitVariableDeclaration(sourceFile, doc, range, indent) {
    const decls = [];
    const text = sourceFile.getFullText();
    visitor(sourceFile);
    return new Promise(resolve => {
        if (decls.length === 0) {
            resolve(undefined);
            return;
        }
        const { varStatement, declName, declInit, declType } = decls[decls.length - 1];
        function resolveChange(initType) {
            const noInitType = !initType;
            initType = initType || 'type';
            const varStatementText = text.substring(varStatement.getStart(), varStatement.getEnd()).trim();
            const declNameText = text.substring(declName.getStart(), declName.getEnd());
            const initText = text.substring(declInit.getStart(), declInit.getEnd());
            const semi = varStatementText.endsWith(';') ? ';' : '';
            const wasConst = varStatementText.startsWith('const');
            const declTypeText = varStatementText.startsWith('var') ? 'var' : 'let';
            const selStartMore = wasConst ? 0 : 2;
            const beforeInitType = `${declTypeText} ${declNameText}: `;
            const newText = `${beforeInitType}${initType}${semi}\n${indent}${declNameText} = ${initText}${semi}`;
            const change = { span: { start: varStatement.getStart(), length: varStatement.getEnd() - varStatement.getStart() }, newText };
            const selection = noInitType ? { start: declName.getEnd() + selStartMore, length: initType.length } : undefined;
            resolve({ change, selection });
        }
        if (declType) {
            resolveChange(text.substring(declType.getStart(), declType.getEnd()));
        }
        else {
            resolveType(declName, doc).then(resolveChange);
        }
    });
    function visitor(node) {
        if (node.kind === ts.SyntaxKind.VariableStatement && refactor_1.inRange(node, range)) {
            const varStatement = node;
            const decl = varStatement.declarationList.declarations[0];
            const declInit = decl.initializer;
            if (declInit) {
                decls.push({ varStatement, declName: decl.name, declType: decl.type, declInit });
            }
        }
        ts.forEachChild(node, visitor);
    }
}
exports.splitVariableDeclaration = splitVariableDeclaration;
//# sourceMappingURL=split-variable-declaration.js.map