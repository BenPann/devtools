"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("./core");
const refactor_1 = require("./refactor");
function toggleSingleStatementBlockExpression() {
    if (!expressionToBlock()) {
        singleStatementBlockToExpression(false);
    }
}
exports.toggleSingleStatementBlockExpression = toggleSingleStatementBlockExpression;
function expressionToBlock() {
    const source = refactor_1.createSourceFileFromActiveEditor();
    if (!source) {
        return false;
    }
    const editor = source.editor;
    const { document, selection } = editor;
    const change = core_1.expressionToBlock(source.sourceFile, refactor_1.selectionToSpan(document, selection), refactor_1.getIndentAtLine(document, selection.start.line), refactor_1.getTabs(editor, 1));
    if (!change) {
        return false;
    }
    editor.edit(builder => builder.replace(refactor_1.changeToRange(document, change), change.newText))
        .then(ok => {
        if (ok) {
            editor.selection = selection;
        }
    });
    return true;
}
exports.expressionToBlock = expressionToBlock;
function singleStatementBlockToExpression(replaceAll) {
    let overlapRecursionsLeft = 10;
    (function doIt() {
        const source = refactor_1.createSourceFileFromActiveEditor();
        if (!source) {
            return;
        }
        const editor = source.editor;
        const { document, selection } = editor;
        const all = core_1.singleStatementBlockToExpressions(source.sourceFile, replaceAll ? undefined : refactor_1.selectionToSpan(document, selection));
        if (all.changes.length === 0) {
            return;
        }
        if (!replaceAll) {
            all.changes = [all.changes[0]];
        }
        editor.edit(builder => all.changes.forEach(change => builder.replace(refactor_1.changeToRange(document, change), change.newText)))
            .then(ok => {
            if (ok) {
                editor.selection = selection;
                if (replaceAll && all.changes.length > 1 && all.overlaps && overlapRecursionsLeft > 0) {
                    doIt();
                    overlapRecursionsLeft--;
                }
            }
        });
    })();
}
exports.singleStatementBlockToExpression = singleStatementBlockToExpression;
//# sourceMappingURL=arrow-function.js.map